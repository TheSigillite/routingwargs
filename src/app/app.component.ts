import { Component } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router){
    this.id = Math.floor((Math.random() * 10) + 1)
  }
  id : number;
  title = 'routingwargs';
  navtoTarget(): void{
    this.router.navigate(['/add' , this.id])
  }
}
