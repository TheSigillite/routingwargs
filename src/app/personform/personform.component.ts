import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-personform',
  templateUrl: './personform.component.html',
  styleUrls: ['./personform.component.css']
})
export class PersonformComponent implements OnInit {

  constructor(private route: ActivatedRoute,private person: FormBuilder) { 
    //recieveing params trough URL
    this.route.params.subscribe(param => {
      this.personForm.patchValue({id: param['id']}),
      console.log(this.personForm.get("id").value)
    })

  }

  personForm = this.person.group({
    id: [''],
    firstName: ['',Validators.compose([Validators.required , Validators.maxLength(255)])],
    lastName: ['',Validators.compose([Validators.required , Validators.maxLength(255)])],
    birthDate: ['',Validators.required],
    wzrost: [''],
    waga: ['']
  })
  ngOnInit() {
    console.log("navigated")
  }

  onSubmit() {
    console.log(this.personForm)
  }

}
