import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonformComponent } from './personform/personform.component'

const routes : Routes = [
  {path: 'add/:id', component: PersonformComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
